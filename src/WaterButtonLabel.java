
public class WaterButtonLabel extends ImageButtonLabel {
	private Plant plant;

	public WaterButtonLabel(Plant plant) {
		super("src/assets/img/water.png", "src/assets/img/water2.png");
		this.plant = plant;
	}
	
	public void restoreDefaultIcon(){
		setIcon(imageActive);
	}
	
	@Override
	protected void clickedAction() {
		if(getIcon() == imageActive){
			plant.water();
			setIcon(imageInactive);
		}
	}

}
