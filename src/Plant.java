import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Plant implements Plantable, Fertilizable {
	private int size;
	private int irrigation;
	private int fertilization;
	private JLabel statsLabel;
	private PlantPanel plantPanel;
	private ImagePanel imagePanel;
	private List<ImageIcon> imageList;
	private HashMap<String, ImageButtonLabel> needs;

	public Plant(PlantPanel plantPanel) {
		this.size = 0;
		this.irrigation = 100;
		this.fertilization = 0;
		this.plantPanel = plantPanel;
		this.needs = new HashMap<String, ImageButtonLabel>();
		this.imageList = new ArrayList<ImageIcon>();
		loadImages();
		this.imagePanel = new ImagePanel(imageList.get(0).getImage());
		this.statsLabel = new JLabel();
		this.statsLabel.setVerticalAlignment(JLabel.TOP);

		updateStats();

		Timer timer = new Timer(2000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generateNeeds();
			}
		});
		timer.start();
	}

	@Override
	public void water() {
		this.irrigation = 100;
		System.out.println("=100 irrgation -> watering your plant!");
	}

	@Override
	public void grow() {
		double growth = 0;
		if (irrigation == 100) { // full grow up
			growth = 100 * (1 + fertilization / 100.);
			System.out.println("1) +" + growth + " size -> your plany is growing up!");
		} else if (irrigation <= 0) { // plant is too dry to grow up
			fertilization = 0;
			growth = -50;
			System.out.println("Your plant is dying! Water it!");
		} else {
			double growthScale = irrigation / 100;
			growth = (int) (size * growthScale * (1 + fertilization / 100));
			System.out.println("2) +" + growth + " size -> your plany is growing up!");
		}
		irrigation -= 80;
		if (fertilization > 0) {
			fertilization -= 5;
		}
		size += growth;
		System.out.println("-50 irrigation -> your plant needs water to live!");
	}

	@Override
	public void fertilize() {
		if (fertilization < 100) {
			fertilization += 20;
		}
	}

	@Override
	public void cleanSoil() {
		fertilization = 0;
	}

	private void generateNeeds() {
		if (needs.size() == 0) {
			initNeeds();
		}

		needs.get("watering").restoreDefaultIcon();
		needs.get("fertilizing").restoreDefaultIcon();
		grow();
	}

	public void updateStats() {
		statsLabel.setText("<html>" + "Plant stats" + "<br>" + "Size: " + size + "<br>" + "Irrigation: " + irrigation
				+ "/100" + "<br>" + "Fertilization: " + fertilization + "<br>" + "</html>");
	}

	public void updateImage() {
		int id = 0;

		if (size > 100 && size < 500) {
			id = 1;
		} else if (size > 500 && size < 1400) {
			id = 2;
		} else if (size > 1400 && size < 2500) {
			id = 3;
		} else if (size > 2500 && size < 4000) {
			id = 4;
		} else if (size >= 4000) {
			id = 5;
		}

		imagePanel.setImage(imageList.get(id).getImage());
		imagePanel.repaint();
	}

	private void initNeeds() {
		WaterButtonLabel watering = new WaterButtonLabel(this);
		FertilizerButtonLabel fertilizing = new FertilizerButtonLabel(this);

		needs.put("watering", watering);
		needs.put("fertilizing", fertilizing);

		plantPanel.add(watering);
		plantPanel.add(fertilizing);
	}

	public void coutToReset(JPanel gameOverPanel) {
		Timer waitingForNewGame = new Timer(5000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				plantPanel.startNewGame(gameOverPanel);
			}
		});
		waitingForNewGame.setRepeats(false);
		waitingForNewGame.start();
	}

	public void reset() {
		this.size = 0;
		this.irrigation = 100;
		System.out.println("NEW GAME STARTED!");
	}

	private void loadImages() {
		for (int i = 0; i < 6; i++) {
			File file = new File("src/assets/img/" + i + ".jpg");
			BufferedImage buffImage;
			try {
				buffImage = ImageIO.read(file);
				ImageIcon image = new ImageIcon(buffImage);
				imageList.add(image);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println(imageList.size() + " images loaded.");
	}

	public JLabel getStatsLabel() {
		return statsLabel;
	}

	public ImagePanel getImagePanel() {
		return imagePanel;
	}

	public int getIrrigarion() {
		return irrigation;
	}

}
