
public class ImageWrongSizeException extends Exception {

	public ImageWrongSizeException(String message) {
		super(message);
	}
}
