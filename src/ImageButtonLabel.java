import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageButtonLabel extends ButtonLabel {
	protected ImageIcon imageActive;
	protected ImageIcon imageInactive;

	public ImageButtonLabel(String urlActive, String urlInactive) {
		super();
		this.imageActive = loadImageActive(urlActive);
		this.imageInactive = loadImageInactive(urlInactive);
		setIcon(imageActive);
	}

	@Override
	protected void clickedAction() {
	}

	public void restoreDefaultIcon() {
	}

	private ImageIcon loadImage(String url) {
		ImageIcon img;
		try {
			BufferedImage buffImg = ImageIO.read(new File(url));
			img = new ImageIcon(buffImg);
		} catch (IOException e) {
			e.printStackTrace();
			img = null;
		}

		return img;
	}

	private ImageIcon loadImageActive(String url) {
		ImageIcon img = loadImage(url);
		return img;
	}

	private ImageIcon loadImageInactive(String url) {
		ImageIcon img = loadImage(url);
		try {
			if (img.getIconHeight() != imageActive.getIconHeight()
					|| img.getIconWidth() != imageActive.getIconWidth()) {
				throw new ImageWrongSizeException("Given images are not in the same size!");
			}
		} catch (ImageWrongSizeException e) {
			System.out.println(e.getMessage());
			img = imageActive;
		}
		return img;
	}
}
