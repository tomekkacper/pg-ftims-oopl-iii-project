
public class FertilizerButtonLabel extends ImageButtonLabel {
	private Plant plant;

	public FertilizerButtonLabel(Plant plant) {
		super("src/assets/img/fertilizer.png", "src/assets/img/fertilizer2.png");
		this.plant = plant;
	}
	
	public void restoreDefaultIcon(){
		setIcon(imageActive);
	}
	
	@Override
	protected void clickedAction() {
		if(getIcon() == imageActive){
			plant.fertilize();
			setIcon(imageInactive);
		}
	}

}
