import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class PlantPanel extends JPanel {

	private JFrame frame;
	private Plant plant;

	public PlantPanel(JFrame frame) {
		plant = new Plant(this);
		this.frame = frame;

		setBackground(Color.WHITE);

		this.add(plant.getStatsLabel());
		this.frame.getContentPane().add(plant.getImagePanel(), BorderLayout.LINE_START);

		Timer timer = new Timer(1000 / 60, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				plant.updateImage();
				plant.updateStats();
				update();

			}
		});
		timer.start();
	}

	public void update() {
		if (plant.getIrrigarion() < -150 && isVisible()) {
			plant.getImagePanel().setVisible(false);
			setVisible(false);

			JPanel gameOverPanel = new JPanel(new BorderLayout());
			JLabel gameOverLabel = new JLabel("GAME OVER", SwingConstants.CENTER);
			JLabel waitTextLabel = new JLabel("wait 5 sec for new game", SwingConstants.CENTER);
			gameOverLabel.setFont(new Font(gameOverLabel.getFont().getName(), Font.PLAIN, 22));
			gameOverPanel.add(gameOverLabel, BorderLayout.CENTER);
			gameOverPanel.add(waitTextLabel, BorderLayout.PAGE_END);
			frame.add(gameOverPanel);

			plant.coutToReset(gameOverPanel);
		}

		repaint();
	}

	public void startNewGame(JPanel gameOverPanel){
		plant.reset();
		plant.updateImage();
		frame.remove(gameOverPanel);
		plant.getImagePanel().setVisible(true);
		setVisible(true);
	}
	
	public JFrame getFrame() {
		return this.frame;
	}

	public Plant getPlant() {
		return this.plant;
	}
}
