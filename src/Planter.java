import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JFrame;

public class Planter {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Planter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);

		PlantPanel plantPanel = new PlantPanel(frame);
		frame.add(plantPanel, BorderLayout.CENTER);

		frame.setSize(800, 500);
		frame.setVisible(true);
	}
}