
public interface Plantable {

	public void water();

	public void grow();

}
