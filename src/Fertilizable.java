
public interface Fertilizable {

	public void fertilize();

	public void cleanSoil();

}
