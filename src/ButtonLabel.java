import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

public abstract class ButtonLabel extends JLabel {
	public ButtonLabel(){
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				clickedAction();
			}
		});
	}
	
	protected abstract void clickedAction();
}
