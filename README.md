# Object-Oriented Programming Languages III final project #

This repository is for students of the Object-Oriented Programming Languages III course final laboratory project. Only through contribution via fork of this repository project will be considered submitted.

# Documentation

* Author: Tomasz Banaś
* This project is a simulator of plant cultivation.
* Compilation: Load repository to your IDE (eg. Eclipse for JEE Developers) and compile project. Should work.
* Usage: Every tour lasts few seconds. During this time user should water and fertilize the plant. The user is able to water and fertilize plant only once per tour by clicking on water drop and fertilizer bag icons. The plant will die if it is not watering. Using the fertilizer increases speed of growing up. There is auto restart after game over.

## Basic project rules

* Each student need to create a Java application using Java SE programming language.
* Projects can be submitted till January 19th, 11:59:59 PM (BitBucket server time). All contributions made after deadline will result in failing project (hence failing course).
* Submitting project before deadline if required to be admitted to oral exam.
* Not submitting project before deadline will result in failing laboratories (hence failing course).
* Subject (what the application actually does) it totally up to student. If for any reason student has a problem with choosing application subject, he or she can ask for assistance project verificators.
* Each student is obliged to create fork of this repository and make all contributions to it. Contributions made to original repository (f.e. pull requests) or send via other channels (like email) won't be accepted.
* Only source code, resources and so should be pushed to repository.
* Each project needs to be documented by README.md file (edited version of this one). Documentation needs to contain:
	* project author name,
	* description of what project does (in just a few sentences),
	* instructions how to compile and run application,
	* all additional information needed for application usage.

## Requirements for project

* All source code needs to be created regarding [Java Code Conventions](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
* Project needs to have at least 2-level class inheritance present (regardless if base class is abstract or not).
* Example of using class field polymorphic binding needs to be present in the source code.
* Usage of at least two custom interfaces is mandatory.
* Project code needs to use two types of collections: List or Set and Map. Collections implementation is for student to decide.
* Example of collection iteration needs to be present in the source code.
* Custom exception needs to be created used in the project.

## Project verificators

* Dr. Jan Franz
* Dr. hab. Julien Guthmuller

## Forking repository

All needed information about forking a repository hosted on BitBucket can be found in the [documentation](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

If there is a problem, please contact project verificators via email or during laboratories.
